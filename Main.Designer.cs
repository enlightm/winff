﻿namespace WinFF
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.InputList = new System.Windows.Forms.ListView();
            this.File = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Format = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.OuputFormat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ButtonStart = new System.Windows.Forms.Button();
            this.ButtonStop = new System.Windows.Forms.Button();
            this.StatusText = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.noRecompressCheck = new System.Windows.Forms.CheckBox();
            this.resolutionCheck = new System.Windows.Forms.CheckBox();
            this.fpsCheck = new System.Windows.Forms.CheckBox();
            this.bitrateCheck = new System.Windows.Forms.CheckBox();
            this.trimCheck = new System.Windows.Forms.CheckBox();
            this.cropCheck = new System.Windows.Forms.CheckBox();
            this.resolutionList = new System.Windows.Forms.ComboBox();
            this.fpsList = new System.Windows.Forms.ComboBox();
            this.bitrateList = new System.Windows.Forms.ComboBox();
            this.trimList = new System.Windows.Forms.ComboBox();
            this.nvencCheck = new System.Windows.Forms.CheckBox();
            this.hevcCheck = new System.Windows.Forms.CheckBox();
            this.trimGroup = new System.Windows.Forms.GroupBox();
            this.trim2 = new System.Windows.Forms.Label();
            this.trim1 = new System.Windows.Forms.Label();
            this.trimText2 = new System.Windows.Forms.TextBox();
            this.trimText1 = new System.Windows.Forms.TextBox();
            this.cropGroup = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cropHeight = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cropWidth = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cropY = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cropX = new System.Windows.Forms.TextBox();
            this.buttonClearQueue = new System.Windows.Forms.Button();
            this.buttonResetQueue = new System.Windows.Forms.Button();
            this.checkNoAudio = new System.Windows.Forms.CheckBox();
            this.encodeBackgroundCheck = new System.Windows.Forms.CheckBox();
            this.remainingTimeLabel = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.encodingPreset = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.bitrateValue = new System.Windows.Forms.TextBox();
            this.bitrateText = new System.Windows.Forms.Label();
            this.PreserveDates = new System.Windows.Forms.CheckBox();
            this.SkipCheckBox = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SkipBitrate = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SkipAndOr = new System.Windows.Forms.ComboBox();
            this.SkipGroup = new System.Windows.Forms.GroupBox();
            this.SkipResolutionCombo = new System.Windows.Forms.ComboBox();
            this.StatusBar.SuspendLayout();
            this.trimGroup.SuspendLayout();
            this.cropGroup.SuspendLayout();
            this.SkipGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // InputList
            // 
            this.InputList.AllowDrop = true;
            this.InputList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.File,
            this.Format,
            this.OuputFormat,
            this.Status});
            this.InputList.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InputList.FullRowSelect = true;
            this.InputList.GridLines = true;
            this.InputList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.InputList.HideSelection = false;
            this.InputList.Location = new System.Drawing.Point(14, 14);
            this.InputList.Margin = new System.Windows.Forms.Padding(12);
            this.InputList.Name = "InputList";
            this.InputList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.InputList.ShowGroups = false;
            this.InputList.Size = new System.Drawing.Size(1362, 405);
            this.InputList.TabIndex = 0;
            this.InputList.UseCompatibleStateImageBehavior = false;
            this.InputList.View = System.Windows.Forms.View.Details;
            this.InputList.DragDrop += new System.Windows.Forms.DragEventHandler(this.InputList_DragDrop);
            this.InputList.DragEnter += new System.Windows.Forms.DragEventHandler(this.InputList_DragEnter);
            this.InputList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InputList_KeyDown);
            // 
            // File
            // 
            this.File.Text = "File";
            this.File.Width = 350;
            // 
            // Format
            // 
            this.Format.Text = "Input Format";
            this.Format.Width = 280;
            // 
            // OuputFormat
            // 
            this.OuputFormat.Text = "Output Format";
            this.OuputFormat.Width = 280;
            // 
            // Status
            // 
            this.Status.Text = "Status";
            this.Status.Width = 100;
            // 
            // ButtonStart
            // 
            this.ButtonStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonStart.Location = new System.Drawing.Point(1008, 599);
            this.ButtonStart.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonStart.Name = "ButtonStart";
            this.ButtonStart.Size = new System.Drawing.Size(181, 82);
            this.ButtonStart.TabIndex = 2;
            this.ButtonStart.Text = "Start";
            this.ButtonStart.UseVisualStyleBackColor = true;
            this.ButtonStart.Click += new System.EventHandler(this.ButtonStart_Click);
            // 
            // ButtonStop
            // 
            this.ButtonStop.Enabled = false;
            this.ButtonStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonStop.Location = new System.Drawing.Point(1196, 599);
            this.ButtonStop.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonStop.Name = "ButtonStop";
            this.ButtonStop.Size = new System.Drawing.Size(181, 82);
            this.ButtonStop.TabIndex = 3;
            this.ButtonStop.Text = "Stop";
            this.ButtonStop.UseVisualStyleBackColor = true;
            this.ButtonStop.Click += new System.EventHandler(this.ButtonStop_Click);
            // 
            // StatusText
            // 
            this.StatusText.Name = "StatusText";
            this.StatusText.Size = new System.Drawing.Size(0, 15);
            // 
            // StatusBar
            // 
            this.StatusBar.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusText});
            this.StatusBar.Location = new System.Drawing.Point(0, 848);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Padding = new System.Windows.Forms.Padding(1, 0, 17, 0);
            this.StatusBar.Size = new System.Drawing.Size(1392, 22);
            this.StatusBar.SizingGrip = false;
            this.StatusBar.TabIndex = 1;
            this.StatusBar.Text = "statusStrip1";
            // 
            // noRecompressCheck
            // 
            this.noRecompressCheck.AutoSize = true;
            this.noRecompressCheck.Location = new System.Drawing.Point(16, 436);
            this.noRecompressCheck.Margin = new System.Windows.Forms.Padding(4);
            this.noRecompressCheck.Name = "noRecompressCheck";
            this.noRecompressCheck.Size = new System.Drawing.Size(170, 24);
            this.noRecompressCheck.TabIndex = 4;
            this.noRecompressCheck.Text = "No Recompression";
            this.noRecompressCheck.UseVisualStyleBackColor = true;
            this.noRecompressCheck.CheckedChanged += new System.EventHandler(this.noRecompressCheck_CheckedChanged);
            // 
            // resolutionCheck
            // 
            this.resolutionCheck.AutoSize = true;
            this.resolutionCheck.Location = new System.Drawing.Point(16, 468);
            this.resolutionCheck.Margin = new System.Windows.Forms.Padding(4);
            this.resolutionCheck.Name = "resolutionCheck";
            this.resolutionCheck.Size = new System.Drawing.Size(111, 24);
            this.resolutionCheck.TabIndex = 5;
            this.resolutionCheck.Text = "Resolution";
            this.resolutionCheck.UseVisualStyleBackColor = true;
            this.resolutionCheck.CheckedChanged += new System.EventHandler(this.resolutionCheck_CheckedChanged);
            // 
            // fpsCheck
            // 
            this.fpsCheck.AutoSize = true;
            this.fpsCheck.Location = new System.Drawing.Point(16, 502);
            this.fpsCheck.Margin = new System.Windows.Forms.Padding(4);
            this.fpsCheck.Name = "fpsCheck";
            this.fpsCheck.Size = new System.Drawing.Size(113, 24);
            this.fpsCheck.TabIndex = 6;
            this.fpsCheck.Text = "Frame rate";
            this.fpsCheck.UseVisualStyleBackColor = true;
            this.fpsCheck.CheckedChanged += new System.EventHandler(this.fpsCheck_CheckedChanged);
            // 
            // bitrateCheck
            // 
            this.bitrateCheck.AutoSize = true;
            this.bitrateCheck.Location = new System.Drawing.Point(16, 534);
            this.bitrateCheck.Margin = new System.Windows.Forms.Padding(4);
            this.bitrateCheck.Name = "bitrateCheck";
            this.bitrateCheck.Size = new System.Drawing.Size(82, 24);
            this.bitrateCheck.TabIndex = 7;
            this.bitrateCheck.Text = "Bitrate";
            this.bitrateCheck.UseVisualStyleBackColor = true;
            this.bitrateCheck.CheckedChanged += new System.EventHandler(this.bitrateCheck_CheckedChanged);
            // 
            // trimCheck
            // 
            this.trimCheck.AutoSize = true;
            this.trimCheck.Location = new System.Drawing.Point(16, 566);
            this.trimCheck.Margin = new System.Windows.Forms.Padding(4);
            this.trimCheck.Name = "trimCheck";
            this.trimCheck.Size = new System.Drawing.Size(65, 24);
            this.trimCheck.TabIndex = 8;
            this.trimCheck.Text = "Trim";
            this.trimCheck.UseVisualStyleBackColor = true;
            this.trimCheck.CheckedChanged += new System.EventHandler(this.trimCheck_CheckedChanged);
            // 
            // cropCheck
            // 
            this.cropCheck.AutoSize = true;
            this.cropCheck.Location = new System.Drawing.Point(16, 599);
            this.cropCheck.Margin = new System.Windows.Forms.Padding(4);
            this.cropCheck.Name = "cropCheck";
            this.cropCheck.Size = new System.Drawing.Size(69, 24);
            this.cropCheck.TabIndex = 9;
            this.cropCheck.Text = "Crop";
            this.cropCheck.UseVisualStyleBackColor = true;
            this.cropCheck.CheckedChanged += new System.EventHandler(this.cropCheck_CheckedChanged);
            // 
            // resolutionList
            // 
            this.resolutionList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resolutionList.FormattingEnabled = true;
            this.resolutionList.Items.AddRange(new object[] {
            "Same as input",
            "50%",
            "25%",
            "Up to 4K",
            "Up to 2K",
            "Up to 1080p",
            "Up to 720p"});
            this.resolutionList.Location = new System.Drawing.Point(163, 468);
            this.resolutionList.Margin = new System.Windows.Forms.Padding(4);
            this.resolutionList.Name = "resolutionList";
            this.resolutionList.Size = new System.Drawing.Size(188, 28);
            this.resolutionList.TabIndex = 10;
            this.resolutionList.SelectedIndexChanged += new System.EventHandler(this.resolutionList_SelectedIndexChanged);
            // 
            // fpsList
            // 
            this.fpsList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fpsList.FormattingEnabled = true;
            this.fpsList.Items.AddRange(new object[] {
            "Same as input",
            "Up to 60",
            "Up to 30",
            "Up to 24",
            "Up to 15"});
            this.fpsList.Location = new System.Drawing.Point(163, 502);
            this.fpsList.Margin = new System.Windows.Forms.Padding(4);
            this.fpsList.Name = "fpsList";
            this.fpsList.Size = new System.Drawing.Size(188, 28);
            this.fpsList.TabIndex = 11;
            this.fpsList.SelectedIndexChanged += new System.EventHandler(this.fpsList_SelectedIndexChanged);
            // 
            // bitrateList
            // 
            this.bitrateList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bitrateList.FormattingEnabled = true;
            this.bitrateList.Items.AddRange(new object[] {
            "Auto (VeryHigh)",
            "Auto (High)",
            "Auto (Default)",
            "Auto (Low)",
            "Same as input",
            "% Percent",
            "Max"});
            this.bitrateList.Location = new System.Drawing.Point(163, 534);
            this.bitrateList.Margin = new System.Windows.Forms.Padding(5);
            this.bitrateList.Name = "bitrateList";
            this.bitrateList.Size = new System.Drawing.Size(188, 28);
            this.bitrateList.TabIndex = 12;
            this.bitrateList.SelectedIndexChanged += new System.EventHandler(this.bitrateList_SelectedIndexChanged);
            // 
            // trimList
            // 
            this.trimList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.trimList.FormattingEnabled = true;
            this.trimList.Items.AddRange(new object[] {
            "Start + Length",
            "Start + End"});
            this.trimList.Location = new System.Drawing.Point(163, 566);
            this.trimList.Margin = new System.Windows.Forms.Padding(4);
            this.trimList.Name = "trimList";
            this.trimList.Size = new System.Drawing.Size(188, 28);
            this.trimList.TabIndex = 13;
            this.trimList.SelectedIndexChanged += new System.EventHandler(this.trimList_SelectedIndexChanged);
            // 
            // nvencCheck
            // 
            this.nvencCheck.AutoSize = true;
            this.nvencCheck.Location = new System.Drawing.Point(492, 468);
            this.nvencCheck.Margin = new System.Windows.Forms.Padding(4);
            this.nvencCheck.Name = "nvencCheck";
            this.nvencCheck.Size = new System.Drawing.Size(345, 24);
            this.nvencCheck.TabIndex = 14;
            this.nvencCheck.Text = "Use NVENC (Faster but may reduce quality)";
            this.nvencCheck.UseVisualStyleBackColor = true;
            // 
            // hevcCheck
            // 
            this.hevcCheck.AutoSize = true;
            this.hevcCheck.Location = new System.Drawing.Point(492, 502);
            this.hevcCheck.Margin = new System.Windows.Forms.Padding(4);
            this.hevcCheck.Name = "hevcCheck";
            this.hevcCheck.Size = new System.Drawing.Size(370, 24);
            this.hevcCheck.TabIndex = 15;
            this.hevcCheck.Text = "Use HEVC (Better for 4K, slow without NVENC)";
            this.hevcCheck.UseVisualStyleBackColor = true;
            this.hevcCheck.CheckedChanged += new System.EventHandler(this.hevcCheck_CheckedChanged);
            // 
            // trimGroup
            // 
            this.trimGroup.Controls.Add(this.trim2);
            this.trimGroup.Controls.Add(this.trim1);
            this.trimGroup.Controls.Add(this.trimText2);
            this.trimGroup.Controls.Add(this.trimText1);
            this.trimGroup.Location = new System.Drawing.Point(491, 604);
            this.trimGroup.Margin = new System.Windows.Forms.Padding(4);
            this.trimGroup.Name = "trimGroup";
            this.trimGroup.Padding = new System.Windows.Forms.Padding(4);
            this.trimGroup.Size = new System.Drawing.Size(235, 100);
            this.trimGroup.TabIndex = 16;
            this.trimGroup.TabStop = false;
            this.trimGroup.Text = "Trim (h:m:s / m:s / s) ";
            // 
            // trim2
            // 
            this.trim2.Location = new System.Drawing.Point(11, 61);
            this.trim2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.trim2.Name = "trim2";
            this.trim2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.trim2.Size = new System.Drawing.Size(71, 23);
            this.trim2.TabIndex = 3;
            this.trim2.Text = "Length";
            this.trim2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // trim1
            // 
            this.trim1.AutoSize = true;
            this.trim1.Location = new System.Drawing.Point(36, 29);
            this.trim1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.trim1.Name = "trim1";
            this.trim1.Size = new System.Drawing.Size(44, 20);
            this.trim1.TabIndex = 2;
            this.trim1.Text = "Start";
            this.trim1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // trimText2
            // 
            this.trimText2.Location = new System.Drawing.Point(89, 58);
            this.trimText2.Margin = new System.Windows.Forms.Padding(4);
            this.trimText2.Name = "trimText2";
            this.trimText2.Size = new System.Drawing.Size(119, 26);
            this.trimText2.TabIndex = 1;
            // 
            // trimText1
            // 
            this.trimText1.Location = new System.Drawing.Point(89, 25);
            this.trimText1.Margin = new System.Windows.Forms.Padding(4);
            this.trimText1.Name = "trimText1";
            this.trimText1.Size = new System.Drawing.Size(119, 26);
            this.trimText1.TabIndex = 0;
            // 
            // cropGroup
            // 
            this.cropGroup.Controls.Add(this.label4);
            this.cropGroup.Controls.Add(this.cropHeight);
            this.cropGroup.Controls.Add(this.label3);
            this.cropGroup.Controls.Add(this.cropWidth);
            this.cropGroup.Controls.Add(this.label1);
            this.cropGroup.Controls.Add(this.cropY);
            this.cropGroup.Controls.Add(this.label2);
            this.cropGroup.Controls.Add(this.cropX);
            this.cropGroup.Location = new System.Drawing.Point(163, 604);
            this.cropGroup.Margin = new System.Windows.Forms.Padding(4);
            this.cropGroup.Name = "cropGroup";
            this.cropGroup.Padding = new System.Windows.Forms.Padding(4);
            this.cropGroup.Size = new System.Drawing.Size(320, 100);
            this.cropGroup.TabIndex = 17;
            this.cropGroup.TabStop = false;
            this.cropGroup.Text = "Crop";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(156, 60);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Height";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cropHeight
            // 
            this.cropHeight.Location = new System.Drawing.Point(216, 58);
            this.cropHeight.Margin = new System.Windows.Forms.Padding(4);
            this.cropHeight.Name = "cropHeight";
            this.cropHeight.Size = new System.Drawing.Size(77, 26);
            this.cropHeight.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(156, 28);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Width";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cropWidth
            // 
            this.cropWidth.Location = new System.Drawing.Point(216, 25);
            this.cropWidth.Margin = new System.Windows.Forms.Padding(4);
            this.cropWidth.Name = "cropWidth";
            this.cropWidth.Size = new System.Drawing.Size(77, 26);
            this.cropWidth.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 61);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Y";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cropY
            // 
            this.cropY.Location = new System.Drawing.Point(65, 59);
            this.cropY.Margin = new System.Windows.Forms.Padding(4);
            this.cropY.Name = "cropY";
            this.cropY.Size = new System.Drawing.Size(77, 26);
            this.cropY.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "X";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cropX
            // 
            this.cropX.Location = new System.Drawing.Point(65, 25);
            this.cropX.Margin = new System.Windows.Forms.Padding(4);
            this.cropX.Name = "cropX";
            this.cropX.Size = new System.Drawing.Size(77, 26);
            this.cropX.TabIndex = 0;
            // 
            // buttonClearQueue
            // 
            this.buttonClearQueue.Location = new System.Drawing.Point(1237, 432);
            this.buttonClearQueue.Margin = new System.Windows.Forms.Padding(4);
            this.buttonClearQueue.Name = "buttonClearQueue";
            this.buttonClearQueue.Size = new System.Drawing.Size(140, 42);
            this.buttonClearQueue.TabIndex = 18;
            this.buttonClearQueue.Text = "Clear queue";
            this.buttonClearQueue.UseVisualStyleBackColor = true;
            this.buttonClearQueue.Click += new System.EventHandler(this.buttonResetQueue_Click);
            // 
            // buttonResetQueue
            // 
            this.buttonResetQueue.Location = new System.Drawing.Point(1088, 432);
            this.buttonResetQueue.Margin = new System.Windows.Forms.Padding(4);
            this.buttonResetQueue.Name = "buttonResetQueue";
            this.buttonResetQueue.Size = new System.Drawing.Size(140, 42);
            this.buttonResetQueue.TabIndex = 19;
            this.buttonResetQueue.Text = "Restart queue";
            this.buttonResetQueue.UseVisualStyleBackColor = true;
            this.buttonResetQueue.Click += new System.EventHandler(this.buttonResetQueue_Click_1);
            // 
            // checkNoAudio
            // 
            this.checkNoAudio.AutoSize = true;
            this.checkNoAudio.Location = new System.Drawing.Point(492, 534);
            this.checkNoAudio.Margin = new System.Windows.Forms.Padding(4);
            this.checkNoAudio.Name = "checkNoAudio";
            this.checkNoAudio.Size = new System.Drawing.Size(100, 24);
            this.checkNoAudio.TabIndex = 20;
            this.checkNoAudio.Text = "No Audio";
            this.checkNoAudio.UseVisualStyleBackColor = true;
            // 
            // encodeBackgroundCheck
            // 
            this.encodeBackgroundCheck.AutoSize = true;
            this.encodeBackgroundCheck.Location = new System.Drawing.Point(1010, 686);
            this.encodeBackgroundCheck.Margin = new System.Windows.Forms.Padding(4);
            this.encodeBackgroundCheck.Name = "encodeBackgroundCheck";
            this.encodeBackgroundCheck.Size = new System.Drawing.Size(316, 24);
            this.encodeBackgroundCheck.TabIndex = 21;
            this.encodeBackgroundCheck.Text = "Encode in background (low CPU usage)";
            this.encodeBackgroundCheck.UseVisualStyleBackColor = true;
            this.encodeBackgroundCheck.CheckedChanged += new System.EventHandler(this.encodeBackgroundCheck_CheckedChanged);
            // 
            // remainingTimeLabel
            // 
            this.remainingTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remainingTimeLabel.Location = new System.Drawing.Point(1012, 486);
            this.remainingTimeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.remainingTimeLabel.Name = "remainingTimeLabel";
            this.remainingTimeLabel.Size = new System.Drawing.Size(366, 73);
            this.remainingTimeLabel.TabIndex = 22;
            this.remainingTimeLabel.Text = "00:00:00";
            this.remainingTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(1009, 564);
            this.progressBar.Margin = new System.Windows.Forms.Padding(4);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(367, 28);
            this.progressBar.TabIndex = 23;
            this.progressBar.Value = 50;
            // 
            // encodingPreset
            // 
            this.encodingPreset.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.encodingPreset.FormattingEnabled = true;
            this.encodingPreset.Items.AddRange(new object[] {
            "Slow (High Quality)",
            "Normal (Default)",
            "Fast (Low Quality)"});
            this.encodingPreset.Location = new System.Drawing.Point(630, 565);
            this.encodingPreset.Margin = new System.Windows.Forms.Padding(4);
            this.encodingPreset.Name = "encodingPreset";
            this.encodingPreset.Size = new System.Drawing.Size(176, 28);
            this.encodingPreset.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(489, 570);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 20);
            this.label5.TabIndex = 25;
            this.label5.Text = "Encoding Preset";
            // 
            // bitrateValue
            // 
            this.bitrateValue.Location = new System.Drawing.Point(361, 534);
            this.bitrateValue.Margin = new System.Windows.Forms.Padding(5);
            this.bitrateValue.Name = "bitrateValue";
            this.bitrateValue.Size = new System.Drawing.Size(102, 26);
            this.bitrateValue.TabIndex = 4;
            this.bitrateValue.TextChanged += new System.EventHandler(this.bitrateValue_TextChanged);
            // 
            // bitrateText
            // 
            this.bitrateText.AutoSize = true;
            this.bitrateText.Location = new System.Drawing.Point(360, 508);
            this.bitrateText.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.bitrateText.Name = "bitrateText";
            this.bitrateText.Size = new System.Drawing.Size(103, 20);
            this.bitrateText.TabIndex = 4;
            this.bitrateText.Text = "Bitrate (kbits)";
            this.bitrateText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PreserveDates
            // 
            this.PreserveDates.AutoSize = true;
            this.PreserveDates.Location = new System.Drawing.Point(16, 633);
            this.PreserveDates.Margin = new System.Windows.Forms.Padding(4);
            this.PreserveDates.Name = "PreserveDates";
            this.PreserveDates.Size = new System.Drawing.Size(144, 24);
            this.PreserveDates.TabIndex = 26;
            this.PreserveDates.Text = "Preserve Dates";
            this.PreserveDates.UseVisualStyleBackColor = true;
            // 
            // SkipCheckBox
            // 
            this.SkipCheckBox.AutoSize = true;
            this.SkipCheckBox.Location = new System.Drawing.Point(16, 665);
            this.SkipCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.SkipCheckBox.Name = "SkipCheckBox";
            this.SkipCheckBox.Size = new System.Drawing.Size(66, 24);
            this.SkipCheckBox.TabIndex = 27;
            this.SkipCheckBox.Text = "Skip";
            this.SkipCheckBox.UseVisualStyleBackColor = true;
            this.SkipCheckBox.CheckedChanged += new System.EventHandler(this.SkipCheckBox_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 32);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 20);
            this.label6.TabIndex = 28;
            this.label6.Text = "<= Bitrate (kbits)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SkipBitrate
            // 
            this.SkipBitrate.Location = new System.Drawing.Point(20, 58);
            this.SkipBitrate.Margin = new System.Windows.Forms.Padding(5);
            this.SkipBitrate.Name = "SkipBitrate";
            this.SkipBitrate.Size = new System.Drawing.Size(122, 26);
            this.SkipBitrate.TabIndex = 29;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(241, 31);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 20);
            this.label7.TabIndex = 30;
            this.label7.Text = "<= Resolution";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SkipAndOr
            // 
            this.SkipAndOr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SkipAndOr.FormattingEnabled = true;
            this.SkipAndOr.Items.AddRange(new object[] {
            "AND",
            "OR"});
            this.SkipAndOr.Location = new System.Drawing.Point(151, 58);
            this.SkipAndOr.Margin = new System.Windows.Forms.Padding(4);
            this.SkipAndOr.Name = "SkipAndOr";
            this.SkipAndOr.Size = new System.Drawing.Size(82, 28);
            this.SkipAndOr.TabIndex = 32;
            // 
            // SkipGroup
            // 
            this.SkipGroup.Controls.Add(this.SkipResolutionCombo);
            this.SkipGroup.Controls.Add(this.SkipAndOr);
            this.SkipGroup.Controls.Add(this.label6);
            this.SkipGroup.Controls.Add(this.label7);
            this.SkipGroup.Controls.Add(this.SkipBitrate);
            this.SkipGroup.Location = new System.Drawing.Point(163, 711);
            this.SkipGroup.Name = "SkipGroup";
            this.SkipGroup.Size = new System.Drawing.Size(408, 108);
            this.SkipGroup.TabIndex = 33;
            this.SkipGroup.TabStop = false;
            this.SkipGroup.Text = "Skip";
            // 
            // SkipResolutionCombo
            // 
            this.SkipResolutionCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SkipResolutionCombo.FormattingEnabled = true;
            this.SkipResolutionCombo.Items.AddRange(new object[] {
            "None",
            "720",
            "1080",
            "2K",
            "4K"});
            this.SkipResolutionCombo.Location = new System.Drawing.Point(241, 58);
            this.SkipResolutionCombo.Margin = new System.Windows.Forms.Padding(4);
            this.SkipResolutionCombo.Name = "SkipResolutionCombo";
            this.SkipResolutionCombo.Size = new System.Drawing.Size(134, 28);
            this.SkipResolutionCombo.TabIndex = 34;
            // 
            // Main
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1392, 870);
            this.Controls.Add(this.PreserveDates);
            this.Controls.Add(this.bitrateText);
            this.Controls.Add(this.bitrateValue);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.SkipCheckBox);
            this.Controls.Add(this.encodingPreset);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.remainingTimeLabel);
            this.Controls.Add(this.encodeBackgroundCheck);
            this.Controls.Add(this.checkNoAudio);
            this.Controls.Add(this.buttonResetQueue);
            this.Controls.Add(this.buttonClearQueue);
            this.Controls.Add(this.cropGroup);
            this.Controls.Add(this.trimGroup);
            this.Controls.Add(this.hevcCheck);
            this.Controls.Add(this.nvencCheck);
            this.Controls.Add(this.trimList);
            this.Controls.Add(this.bitrateList);
            this.Controls.Add(this.fpsList);
            this.Controls.Add(this.resolutionList);
            this.Controls.Add(this.cropCheck);
            this.Controls.Add(this.trimCheck);
            this.Controls.Add(this.bitrateCheck);
            this.Controls.Add(this.fpsCheck);
            this.Controls.Add(this.resolutionCheck);
            this.Controls.Add(this.noRecompressCheck);
            this.Controls.Add(this.ButtonStop);
            this.Controls.Add(this.ButtonStart);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.InputList);
            this.Controls.Add(this.SkipGroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "Main";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "WinFF - By Federico Oro Vojacek";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.StatusBar.ResumeLayout(false);
            this.StatusBar.PerformLayout();
            this.trimGroup.ResumeLayout(false);
            this.trimGroup.PerformLayout();
            this.cropGroup.ResumeLayout(false);
            this.cropGroup.PerformLayout();
            this.SkipGroup.ResumeLayout(false);
            this.SkipGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView InputList;
        private System.Windows.Forms.ColumnHeader File;
        private System.Windows.Forms.ColumnHeader Format;
        private System.Windows.Forms.ColumnHeader Status;
        private System.Windows.Forms.Button ButtonStart;
        private System.Windows.Forms.Button ButtonStop;
        private System.Windows.Forms.ColumnHeader OuputFormat;
        private System.Windows.Forms.ToolStripStatusLabel StatusText;
        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.CheckBox noRecompressCheck;
        private System.Windows.Forms.CheckBox resolutionCheck;
        private System.Windows.Forms.CheckBox fpsCheck;
        private System.Windows.Forms.CheckBox bitrateCheck;
        private System.Windows.Forms.CheckBox trimCheck;
        private System.Windows.Forms.CheckBox cropCheck;
        private System.Windows.Forms.ComboBox resolutionList;
        private System.Windows.Forms.ComboBox fpsList;
        private System.Windows.Forms.ComboBox bitrateList;
        private System.Windows.Forms.ComboBox trimList;
        private System.Windows.Forms.CheckBox nvencCheck;
        private System.Windows.Forms.CheckBox hevcCheck;
        private System.Windows.Forms.GroupBox trimGroup;
        private System.Windows.Forms.Label trim2;
        private System.Windows.Forms.Label trim1;
        private System.Windows.Forms.TextBox trimText2;
        private System.Windows.Forms.TextBox trimText1;
        private System.Windows.Forms.GroupBox cropGroup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox cropHeight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox cropWidth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox cropY;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox cropX;
        private System.Windows.Forms.Button buttonClearQueue;
        private System.Windows.Forms.Button buttonResetQueue;
        private System.Windows.Forms.CheckBox checkNoAudio;
        private System.Windows.Forms.CheckBox encodeBackgroundCheck;
        private System.Windows.Forms.Label remainingTimeLabel;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.ComboBox encodingPreset;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox bitrateValue;
        private System.Windows.Forms.Label bitrateText;
        private System.Windows.Forms.CheckBox PreserveDates;
        private System.Windows.Forms.CheckBox SkipCheckBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox SkipBitrate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox SkipAndOr;
        private System.Windows.Forms.GroupBox SkipGroup;
        private System.Windows.Forms.ComboBox SkipResolutionCombo;
    }
}

