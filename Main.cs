﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Text.Json;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace WinFF
{
    public partial class Main : Form
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool AttachConsole(uint dwProcessId);

        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        static extern bool FreeConsole();

        [DllImport("kernel32.dll")]
        static extern bool SetConsoleCtrlHandler(ConsoleCtrlDelegate HandlerRoutine, bool Add);

        delegate bool ConsoleCtrlDelegate(int CtrlType);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool GenerateConsoleCtrlEvent(int sigevent, int dwProcessGroupId);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();

#pragma warning disable IDE1006 // Naming Styles
        public class JsonStream
        {
            public string codec_name { get; set; }
            public int width { get; set; }
            public int height { get; set; }
            public string r_frame_rate { get; set; }
            public string bit_rate { get; set; }
            public string nb_frames { get; set; }
        }   

        public class JsonFormat
        {
            public string bit_rate { get; set; }
            public string duration { get; set; }
        }

        public class JsonProbe
        {
            public List<JsonStream> streams { get; set; }
            public JsonFormat format { get; set; }
        }

        public class JsonConfig
        {
            public bool no_recompress { get; set; }
            public bool resolution { get; set; }
            public int resolutionIndex { get; set; }
            public bool framerate { get; set; }
            public int framerateIndex { get; set; }
            public bool bitrate { get; set; }
            public int bitrateIndex { get; set; }
            public bool trim { get; set; }
            public int trimIndex { get; set; }
            public string trimStart { get; set; }
            public string trimEnd { get; set; }
            public bool crop { get; set; }
            public int cropX { get; set; }
            public int cropY { get; set; }
            public int cropW { get; set; }
            public int cropH { get; set; }
            public bool nvenc { get; set; }
            public bool hevc { get; set; }
            public bool noaudio { get; set; }
            public int preset { get; set; }
            public bool preserveDates { get; set; }
            public int bitratePercent  { get; set; }
            public Int64 bitrateValue  { get; set; }
            public bool Skip { get; set; }
            public int SkipResolutionIndex { get; set; }
            public int SkipAndOrIndex { get; set; }
            public int SkipBitrate { get; set; }

            public JsonConfig()
            {
                // Defaults
                preset = 1;
                bitrateValue = 8000 * 1000;
                bitratePercent = 100;
            }
        }

#pragma warning restore IDE1006 // Naming Styles

        public enum EVideoState
        {
            Ready,
            Processing,
            Error,
            Completed,
            Cancelled,
            Skipped,
        }

        private Process EncodingProcess;
        private Video EncodingInputVideo;
        private Video EncodingOutputVideo;
        private Timer QueueTimer;
        private int QueueTotalFrames;

        private JsonConfig Config = new JsonConfig();
        private string ConfigPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),"WinFF","WinFF.cfg");

        public class Video
        {
            public string Codec;
            public int Width;
            public int Height;
            public float FrameRate;
            public Int64 Bitrate;
            public string FilePath;
            public EVideoState State;
            public int StartSeconds;
            public int LengthSeconds;
            public int FrameCount;

            public DateTime LastWriteTime;
            public DateTime LastAccessTime;
            public DateTime CreationTime;

            public void Copy(ref Video Out)
            {
                Out.Codec = string.Copy(Codec);
                Out.Width = Width;
                Out.Height = Height;
                Out.FrameRate = FrameRate;
                Out.Bitrate = Bitrate;
                Out.FilePath = string.Copy(FilePath);
                Out.State = State;
                Out.StartSeconds = StartSeconds;
                Out.LengthSeconds = LengthSeconds;
                Out.FrameCount = FrameCount;
                Out.LastWriteTime = LastWriteTime;
                Out.LastAccessTime = LastAccessTime;
                Out.CreationTime = CreationTime;
            }
        }


        string LastEncoderStatus;
        int CurrentEncodingFrame;
        float CurrentEncodingFPS;
        MovingAverage CurrentEncodingFPS_MA;
        object StatusLock = new object();

        private string ffprobeBin = "ffprobe";
        private string ffmpegBin = "ffmpeg";

        public Main()
        {
            InitializeComponent();

            // Defaults
            StatusText.Text = "Ready";
            remainingTimeLabel.Visible = false;
            remainingTimeLabel.Text = "";
            progressBar.Visible = false;
            encodingPreset.SelectedIndex = 1;

            // Config
            LoadConfig();

            foreach (var ext in new[] {".dll", ".exe"})
            {
                if (System.IO.File.Exists(ffprobeBin + ext))
                    ffprobeBin = ffprobeBin + ext;
                if (System.IO.File.Exists(ffmpegBin + ext))
                    ffmpegBin = ffmpegBin + ext;
            }

            if (!System.IO.File.Exists(ffprobeBin) || !System.IO.File.Exists(ffmpegBin))
            {
                if (MessageBox.Show(
                    $"Please copy {ffprobeBin} and {ffmpegBin} in the same folder as WinFF.\nDo you want to open https://www.ffmpeg.org/download.html ?",
                    "FFmpeg binaries required",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    Process.Start("https://www.ffmpeg.org/download.html");
                }
                Environment.Exit(1);
            }
        }

        private bool IsSafeToOverwrite()
        {
            Video Output = new Video();
            foreach (ListViewItem Item in InputList.Items)
            {
                Video V = (Video)Item.Tag;
                if (CanProcessVideo(V))
                {
                    string Path = GetOutputVideo(V, Output).FilePath;
                    if (System.IO.File.Exists(Path))
                    {
                        return MessageBox.Show("One or more output files already exist. Overwrite ?", "Output files",
                            MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2) == DialogResult.Yes;
                    }
                }
            }
            return true;
        }

        private void LoadConfig()
        {
            try
            {
                string ConfigString = System.IO.File.ReadAllText(ConfigPath);
                if (ConfigString.Length > 0)
                {
                    Config = JsonSerializer.Deserialize<JsonConfig>(ConfigString);
                }
            }
            catch (Exception) { }

            noRecompressCheck.Checked = Config.no_recompress;

            resolutionCheck.Checked = Config.resolution;
            resolutionList.SelectedIndex = Config.resolutionIndex;

            fpsCheck.Checked = Config.framerate;
            fpsList.SelectedIndex = Config.framerateIndex;

            bitrateCheck.Checked = Config.bitrate;
            bitrateList.SelectedIndex = Config.bitrateIndex;

            trimCheck.Checked = Config.trim;
            trimList.SelectedIndex = Config.trimIndex;

            trimText1.Text = Config.trimStart;
            trimText2.Text = Config.trimEnd;

            cropCheck.Checked = Config.crop;
            cropX.Text = Config.cropX.ToString();
            cropY.Text = Config.cropY.ToString();
            cropWidth.Text = Config.cropW.ToString();
            cropHeight.Text = Config.cropH.ToString();

            nvencCheck.Checked = Config.nvenc;
            hevcCheck.Checked = Config.hevc;
            PreserveDates.Checked = Config.preserveDates;
            checkNoAudio.Checked = Config.noaudio;
            encodingPreset.SelectedIndex = Config.preset;

            SkipCheckBox.Checked = Config.Skip;
            SkipBitrate.Text = Config.SkipBitrate.ToString();
            SkipAndOr.SelectedIndex = Config.SkipAndOrIndex;
            SkipResolutionCombo.SelectedIndex = Config.SkipResolutionIndex;

            RefreshUI();
        }

        private void RefreshUI()
        {
            resolutionCheck_CheckedChanged(null,null);
            fpsCheck_CheckedChanged(null,null);
            bitrateCheck_CheckedChanged(null,null);
            trimCheck_CheckedChanged(null,null);
            cropCheck_CheckedChanged(null,null);
            noRecompressCheck_CheckedChanged(null,null);
            SkipCheckBox_CheckedChanged(null,null);
        }

        private void SaveConfig()
        {
            Config.no_recompress = noRecompressCheck.Checked;
            Config.resolution = resolutionCheck.Checked;
            Config.resolutionIndex = resolutionList.SelectedIndex;
            Config.framerate = fpsCheck.Checked;
            Config.framerateIndex = fpsList.SelectedIndex;
            Config.bitrate = bitrateCheck.Checked;
            Config.bitrateIndex = bitrateList.SelectedIndex;
            Config.trim = trimCheck.Checked;
            Config.trimIndex = trimList.SelectedIndex;
            Config.trimStart = trimText1.Text;
            Config.trimEnd = trimText2.Text;
            Config.crop = cropCheck.Checked;
            Config.nvenc = nvencCheck.Checked;
            Config.hevc = hevcCheck.Checked;
            Config.noaudio = checkNoAudio.Checked;
            Config.preset = encodingPreset.SelectedIndex;
            Config.preserveDates = PreserveDates.Checked;

            Config.Skip = SkipCheckBox.Checked;
            Config.SkipAndOrIndex = SkipAndOr.SelectedIndex;
            Config.SkipResolutionIndex = SkipResolutionCombo.SelectedIndex;

            int value;
            if (int.TryParse(cropX.Text,out value)) Config.cropX = value;
            if (int.TryParse(cropY.Text,out value)) Config.cropY = value;
            if (int.TryParse(cropWidth.Text,out value)) Config.cropW = value;
            if (int.TryParse(cropHeight.Text,out value)) Config.cropH = value;
            if (int.TryParse(SkipBitrate.Text,out value)) Config.SkipBitrate = value;

            string ConfigString = JsonSerializer.Serialize<JsonConfig>(Config);
            if (ConfigString.Length > 0)
            {
                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(ConfigPath));
                System.IO.File.WriteAllText(ConfigPath, ConfigString);
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveConfig();
            StopEncodingProcess();
        }

        private int ParseTrimTimeStringToSeconds(string HMS)
        {
            try
            {
                string[] f = HMS.Split(':');
                if (f.Length == 3)
                {
                    return int.Parse(f[0]) * 60 * 60 + int.Parse(f[1]) * 60 + int.Parse(f[2]);
                }
                else if (f.Length == 2)
                {
                    return int.Parse(f[0]) * 60 + int.Parse(f[1]);

                }
                else if (f.Length == 1)
                {
                    return int.Parse(f[0]);
                }
            }
            catch (Exception)
            { }
            return -1; // Invalid
        }

        private void UpdateEncoderStatus(string data)
        {
            lock (StatusLock)
            {
                if (data == null || data.Length == 0)
                    return;

                List<string> splits = data.Split(new Char[] { ' ', '=' }).ToList();
                splits.RemoveAll(x => x.Length == 0);
                if (splits.Count % 2 != 0)
                    return;

                if (splits[0] != "frame")
                    return;

                LastEncoderStatus = "";
                for (int i = 0; i < splits.Count; i += 2)
                {
                    if (splits[i] == "frame")
                    {
                        CurrentEncodingFrame = 0;
                        int.TryParse(splits[i + 1],out CurrentEncodingFrame);
                    } else  if (splits[i] == "fps")
                    {
                        CurrentEncodingFPS = 0;
                        float.TryParse(splits[i + 1],out CurrentEncodingFPS);
                    }
                    LastEncoderStatus += splits[i] + "=" + splits[i + 1];
                    LastEncoderStatus += " ";
                }
            }

        }

        private int GetVideoFrames(Video video)
        {
            if (video.LengthSeconds > 0)
            {
                // Trimmed values
                return (int)Math.Round(video.LengthSeconds * video.FrameRate);
            }
            // Total frame count
            return video.FrameCount;
        }

        private float EstimateRemainingEncodingTime(int ProcessedFrames, Video video)
        {
            int RemainingFrames = Math.Max(0, GetVideoFrames(video) - ProcessedFrames);

            float AverageFPS = 0;

            if (CurrentEncodingFPS_MA != null)
            {
                AverageFPS = CurrentEncodingFPS_MA.Average;
            }

            if (AverageFPS > 0)
                return RemainingFrames / AverageFPS;

            return 0;
        }

        private string GetVideoFormatString(Video V)
        {
            if (V.Bitrate > 0)
            {
                return String.Format("{0}x{1}@{2} {3:0.##}Mbps [{4}]", V.Width, V.Height, V.FrameRate, V.Bitrate / 1000000.0, V.Codec);
            }
            else
            {
                return String.Format("{0}x{1}@{2} [{3}]", V.Width, V.Height, V.FrameRate, V.Codec);
            }
        }

        private Video GetOutputVideo(Video InputVideo,Video OutputVideo)
        {
            InputVideo.Copy(ref OutputVideo);

            if (cropCheck.Checked)
            {
                int.TryParse(cropWidth.Text, out OutputVideo.Width);
                int.TryParse(cropHeight.Text, out OutputVideo.Height);
            }

            if (!noRecompressCheck.Checked)
            {
                switch (resolutionList.SelectedIndex)
                {
                    case 0: // Same as input
                        break;
                    case 1: // 50%
                        OutputVideo.Width /= 2;
                        OutputVideo.Height /= 2;
                        break;
                    case 2: // %25
                        OutputVideo.Width /= 4;
                        OutputVideo.Height /= 4;
                        break;
                    case 3: // 4K
                        if (OutputVideo.Height > 2160)
                        {
                            float Scale = (float)OutputVideo.Height / 2160.0f;
                            OutputVideo.Width = (int)((float)OutputVideo.Width / Scale);
                            OutputVideo.Height = 2160;
                        }
                        break;
                    case 4: // 2K
                        if (OutputVideo.Height > 1440)
                        {
                            float Scale = (float)OutputVideo.Height / 1440.0f;
                            OutputVideo.Width = (int)((float)OutputVideo.Width / Scale);
                            OutputVideo.Height = 1440;
                        }
                        break;
                    case 5: // 1080
                        if (OutputVideo.Height > 1080)
                        {
                            float Scale = (float)OutputVideo.Height / 1080.0f;
                            OutputVideo.Width = (int)((float)OutputVideo.Width / Scale);
                            OutputVideo.Height = 1080;
                        }
                        break;
                    case 6: // 720
                        if (OutputVideo.Height > 720)
                        {
                            float Scale = (float)OutputVideo.Height / 720.0f;
                            OutputVideo.Width = (int)((float)OutputVideo.Width / Scale);
                            OutputVideo.Height = 720;
                        }
                        break;
                }

                switch (fpsList.SelectedIndex)
                {
                    case 0: // Same as input
                        break;
                    case 1: // Up to 60
                        OutputVideo.FrameRate = Math.Min(OutputVideo.FrameRate,60);
                        break;
                    case 2: // Up to 30
                        OutputVideo.FrameRate = Math.Min(OutputVideo.FrameRate,30);
                        break;
                    case 3: // Up to 24
                        OutputVideo.FrameRate = Math.Min(OutputVideo.FrameRate,24);
                        break;
                    case 4: // Up to 15
                        OutputVideo.FrameRate = Math.Min(OutputVideo.FrameRate,15);
                        break;
                }

                if (bitrateCheck.Checked)
                {
                    switch (bitrateList.SelectedIndex)
                    {
                        case 0: // Auto
                        case 1: // Auto
                        case 2: // Auto
                        case 3: // Auto
                            OutputVideo.Bitrate = 0;
                            break;
                        case 4: // Same as input
                            break;
                        case 5: // %
                            OutputVideo.Bitrate = (OutputVideo.Bitrate * Config.bitratePercent) / 100;
                            break;
                        case 6: // Max
                            OutputVideo.Bitrate = Math.Min(OutputVideo.Bitrate, Config.bitrateValue);
                            break;
                    }
                } else
                {
                    OutputVideo.Bitrate = 0;
                }

                if (hevcCheck.Checked)
                    OutputVideo.Codec = "HEVC";
            }

            // Trim
            if (trimCheck.Checked)
            {
                switch (trimList.SelectedIndex)
                {
                    case 0: // Start + Length
                        OutputVideo.StartSeconds = ParseTrimTimeStringToSeconds(trimText1.Text);
                        OutputVideo.LengthSeconds = ParseTrimTimeStringToSeconds(trimText2.Text);
                        break;
                    case 1: // Start + End
                        OutputVideo.StartSeconds = ParseTrimTimeStringToSeconds(trimText1.Text);
                        OutputVideo.LengthSeconds = ParseTrimTimeStringToSeconds(trimText2.Text) - OutputVideo.StartSeconds;
                        break;
                }
            }

            OutputVideo.FrameCount = (int)Math.Round((InputVideo.FrameCount * OutputVideo.FrameRate) / InputVideo.FrameRate);

            OutputVideo.FilePath = Path.GetDirectoryName(InputVideo.FilePath) + "\\" + Path.GetFileNameWithoutExtension(InputVideo.FilePath) + "_out.mp4";

            OutputVideo.LastWriteTime = InputVideo.LastWriteTime;
            OutputVideo.LastAccessTime = InputVideo.LastAccessTime;
            OutputVideo.CreationTime = InputVideo.CreationTime;

            return OutputVideo;
        }

        private void AddVideoToList(Video V)
        {
            Video Output = new Video();
            var Item = InputList.Items.Add(V.FilePath);
            Item.Tag = V;
            Item.SubItems.Add(GetVideoFormatString(V)); // Input Format
            Item.SubItems.Add(GetVideoFormatString(GetOutputVideo(V,Output))); // Output Format
            Item.SubItems.Add(V.State.ToString());
        }

        private void RefreshVideoDetails(Video V, bool bHighlight = false)
        {
            if (V == null)
                return;
            Video Output = new Video();
            foreach (ListViewItem Item in InputList.Items)
            {
                if (V == (Video)Item.Tag)
                {
                    Item.SubItems.Clear();
                    Item.Text = V.FilePath;
                    Item.SubItems.Add(GetVideoFormatString(V)); // Input Format
                    Item.SubItems.Add(GetVideoFormatString(GetOutputVideo(V,Output))); // Output Format
                    Item.SubItems.Add(V.State.ToString());
                    Item.Font = new System.Drawing.Font(Item.Font, bHighlight ? System.Drawing.FontStyle.Bold : System.Drawing.FontStyle.Regular);
                    return;
                }
            }
        }

        private void InputList_DragDrop(object sender, DragEventArgs e)
        {
            string[] InputFiles = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            string Params = "-v quiet -select_streams v:0 -show_entries stream=nb_frames,width,height,r_frame_rate,bit_rate,codec_name -show_entries format=duration,bit_rate -of json";

            foreach (string FilePath in InputFiles)
            {
                try
                {
                    var process = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            FileName = ffprobeBin,
                            Arguments = Params + " " + "\"" + FilePath + "\"",
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                            CreateNoWindow = true
                        }
                    };

                    if (!process.Start())
                    {
                        continue;
                    }

                    process.WaitForExit();
                    string Json = process.StandardOutput.ReadToEnd();
                    if (Json.Length == 0)
                        continue;

                    JsonProbe vf = JsonSerializer.Deserialize<JsonProbe>(Json);
                    JsonStream Stream = vf.streams[0];

                    string[] RateDiv = Stream.r_frame_rate.Split('/');
                    float FrameRate = 0;
                    if (RateDiv.Length == 2)
                    {
                        FrameRate = float.Parse(RateDiv[0]) / float.Parse(RateDiv[1]);
                        Stream.r_frame_rate = FrameRate.ToString("0.00");
                    }
                    else
                    {
                        throw new System.Exception();
                    }

                    Video V = new Video();
                    if (Stream.bit_rate != null)
                    {
                        V.Bitrate = int.Parse(Stream.bit_rate);
                    } else if (vf.format.bit_rate != null)
                    {
                        V.Bitrate = int.Parse(vf.format.bit_rate);
                    }

                    V.FilePath = FilePath;
                    V.Width = Stream.width;
                    V.Height = Stream.height;
                    V.FrameRate = FrameRate;
                    V.Codec = Stream.codec_name;
                    V.State = EVideoState.Ready;

                    FileInfo fileInfo = new FileInfo(FilePath);
                    V.LastWriteTime = fileInfo.LastWriteTime;
                    V.LastAccessTime = fileInfo.LastAccessTime;
                    V.CreationTime = fileInfo.CreationTime;

                    if (Stream.nb_frames != null)
                    {
                        V.FrameCount = int.Parse(Stream.nb_frames);
                    } else if (vf.format.duration != null)
                    {
                        V.FrameCount = (int)(float.Parse(vf.format.duration) * FrameRate);
                    }
                    AddVideoToList(V);
                }
                catch (Exception)
                {
                }

                if (InputList.Items.Count > 0)
                    StatusText.Text = InputList.Items.Count.ToString() + " items added.";
            }
        }

        private void InputList_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;

        }

        private void InputList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                foreach (ListViewItem Item in InputList.SelectedItems)
                {
                    InputList.Items.Remove(Item);
                }
            }
        }

        private string BuildArguments(Video input, Video output)
        {
            string args = "-y";

            if (noRecompressCheck.Checked)
            {
                args += " -vcodec copy -acodec copy -y";
            }
            else
            {
                string vf = "";

                if (nvencCheck.Checked)
                {
                    if (hevcCheck.Checked)
                        args += " -c:v hevc_nvenc";
                    else
                        args += " -c:v h264_nvenc";
                }
                else
                {
                    if (hevcCheck.Checked)
                        args += " -c:v libx265";
                    else 
                        args += " -c:v libx264";
                }

                if (output.Bitrate != 0) // 0 = Auto
                {
                    // override bitrate
                    args += " -b:v " + output.Bitrate / 1000 + "k";
                } else
                {
                    // Quality output (Constant Rate Factor)
                    switch(bitrateList.SelectedIndex)
                    {
                        case 0: // Very High Quality
                            args += " -crf 19";
                            break;
                        case 1: // High Quality
                            args += " -crf 22";
                            break;
                        case 2: // Default
                            args += " -crf 23";
                            break;
                        case 3: // Low Quality
                            args += " -crf 30";
                            break;
                    }
                }

                if (input.FrameRate != output.FrameRate)
                {
                    // override fps
                    if (vf.Length > 0)
                        vf += ",";
                    vf += "fps=" + output.FrameRate;
                }

                if (cropCheck.Checked)
                {
                    int value;
                    if (int.TryParse(cropWidth.Text, out value) &&
                    int.TryParse(cropHeight.Text, out value) &&
                    int.TryParse(cropX.Text, out value) &&
                    int.TryParse(cropY.Text, out value))
                    {
                        if (vf.Length > 0)
                            vf += ",";
                        vf += "crop=" + cropWidth.Text + ":" + cropHeight.Text + ":" + cropX.Text + ":" + cropY.Text;
                    }
                }

                if (input.Width != output.Width || input.Height != output.Height)
                {
                    // override res
                    if (vf.Length > 0)
                        vf += ",";
                    vf += "scale=" + output.Width + "x" + output.Height;
                }

                if (vf.Length > 0)
                    args += " -vf \"" + vf + "\"";

            }

            if (checkNoAudio.Checked)
                args += " -an";
            if (output.StartSeconds > 0)
                args += " -ss " + output.StartSeconds;
            if (output.LengthSeconds > 0)
                args += " -t " + output.LengthSeconds;

            if (encodeBackgroundCheck.Checked)
                args += " -threads 1";

            switch(encodingPreset.SelectedIndex)
            {
                case 0: // slow
                    args += " -preset slow";
                    break;
                case 2: // fast
                    args += " -preset fast";
                    break;
            }

            return args;
        }

        private void HighlightVideoInInputList(Video video)
        {
            foreach (ListViewItem Item in InputList.Items)
            {
                if (video == (Video)Item.Tag)
                {
                    Item.Font = new System.Drawing.Font(Item.Font, System.Drawing.FontStyle.Bold);
                    return;
                }
            }
        }

        private void StartEncodingProcess(Video video)
        {
            StopEncodingProcess();
            CurrentEncodingFPS = 0;
            CurrentEncodingFrame = 0;

            StatusText.Text = "Processing " + Path.GetFileName(video.FilePath) + "...";

            EncodingInputVideo = video;
            EncodingOutputVideo = new Video();
            GetOutputVideo(video,EncodingOutputVideo);
            EncodingInputVideo.State = EVideoState.Processing;

            string inputPath = video.FilePath;
            string args = BuildArguments(EncodingInputVideo, EncodingOutputVideo);
            string outputPath = EncodingOutputVideo.FilePath;

            EncodingProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = ffmpegBin,
                    Arguments = "-i \"" + inputPath + "\" " + args + " \"" + outputPath + "\"",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true
                }
            };

            EncodingProcess.OutputDataReceived += (s, a) => UpdateEncoderStatus(a.Data);
            EncodingProcess.ErrorDataReceived += (s, a) => UpdateEncoderStatus(a.Data);

            EncodingProcess.Start();
            EncodingProcess.BeginOutputReadLine();
            EncodingProcess.BeginErrorReadLine();

            SetEncodingProcessPriority();
        }

        private bool IsEncodingProcessCompletedSuccessfully()
        {
            return EncodingProcess != null && EncodingProcess.HasExited && EncodingProcess.ExitCode == 0;
        }

        private bool IsEncodingProcessRunning()
        {
            return EncodingProcess != null && !EncodingProcess.HasExited;
        }

        public bool StopProgram(Process P, int WaitMS)
        {
            // Send Ctrl+C to process console
            if (AttachConsole((uint)P.Id))
            {
                SetConsoleCtrlHandler(null, true);
                GenerateConsoleCtrlEvent( /* Ctrl+C */ 0, 0);
                FreeConsole();
                P.WaitForExit(WaitMS);
                SetConsoleCtrlHandler(null, false);
            }
            return P.HasExited;
        }

        private void StopEncodingProcess()
        {
            if (IsEncodingProcessRunning())
            {
                if (!StopProgram(EncodingProcess, 5000))
                    EncodingProcess.Kill();
            }
            if (EncodingInputVideo != null)
            {
                if (EncodingInputVideo.State == EVideoState.Processing)
                    EncodingInputVideo.State = EVideoState.Cancelled;

                RefreshVideoDetails(EncodingInputVideo);
            }
            EncodingInputVideo = null;
        }

        private void StartQueue()
        {
            if (!IsSafeToOverwrite())
                return;

            StopQueue();

            QueueTotalFrames = GetQueueTotalFrames();
            progressBar.Value = 0;
            progressBar.Visible = true;

            CurrentEncodingFPS_MA = new MovingAverage();

            remainingTimeLabel.Visible = true;
            remainingTimeLabel.Text = "";

            StatusText.Text = "Starting queue...";
            ButtonStart.Enabled = false;
            ButtonStop.Enabled = true;

            EnableUI(false);

            QueueTimer = new Timer();
            QueueTimer.Tick += new EventHandler(OnTimerQueueUpdate);
            QueueTimer.Interval = 500;
            QueueTimer.Start();
        }

        private bool IsQueueRunning()
        {
            return IsEncodingProcessRunning() || (QueueTimer != null && QueueTimer.Enabled);
        }

        private void StopQueue()
        {
            ButtonStop.Enabled = false;
            StopEncodingProcess();
            if (QueueTimer != null)
            {
                StatusText.Text = "Stopped.";
                QueueTimer.Stop();
                QueueTimer = null;
            }

            QueueTotalFrames = 0;
            remainingTimeLabel.Visible = false;
            remainingTimeLabel.Text = "";
            progressBar.Visible = false;
            progressBar.Value = 0;

            ButtonStart.Enabled = true;
            ButtonStop.Enabled = false;
            EnableUI(true);
        }

        private bool CanProcessVideo(Video V)
        {
            return V != null && (V.State == EVideoState.Ready || V.State == EVideoState.Cancelled);
        }

        private bool ShouldSkipVideo(Video V)
        {
            // Skip filters are disabled
            if (!SkipCheckBox.Checked)
                return false;

            bool bSkipBitrateCondition = false;

            int SkipBitrateKB = 0;
            if (int.TryParse(SkipBitrate.Text, out SkipBitrateKB))
            {
                if (SkipBitrateKB > 0 && (V.Bitrate/1000) <= SkipBitrateKB)
                {
                    bSkipBitrateCondition = true;
                }
            }

            bool bSkipResolutionCondition = false;
            int SkipHeight = 0;
            switch (SkipResolutionCombo.SelectedIndex)
            {
                case 1: SkipHeight = 720; break;
                case 2: SkipHeight = 1080; break;
                case 3: SkipHeight = 1440; break;
                case 4: SkipHeight = 2160; break;
            }
            if (SkipHeight > 0 && V.Height <= SkipHeight)
            {
                bSkipResolutionCondition = true;
            }

            if (SkipAndOr.SelectedIndex == 0) // AND
            {
                return bSkipBitrateCondition && bSkipResolutionCondition;
            } else if (SkipAndOr.SelectedIndex == 1) // OR
            {
                return bSkipBitrateCondition || bSkipResolutionCondition;
            }
            return false;
        }
        private Video GetNextVideoToProcess()
        {
            foreach (ListViewItem Item in InputList.Items)
            {
                Video V = (Video)Item.Tag;

                if (ShouldSkipVideo(V))
                {
                    V.State = EVideoState.Skipped;
                    RefreshVideoDetails(V);
                    continue;
                }

                if (CanProcessVideo(V))
                {
                    return V;
                }
            }
            return null;
        }

        float GetQueueEstimatedTime()
        {
            float TotalSeconds = 0;
            Video OutputVideo = new Video();
            foreach (ListViewItem Item in InputList.Items)
            {
                Video V = (Video)Item.Tag;
                if (V == EncodingInputVideo)
                {
                    // Current Video
                    TotalSeconds += EstimateRemainingEncodingTime( CurrentEncodingFrame, EncodingOutputVideo );
                    continue;
                }
                if (CanProcessVideo(V))
                {
                    TotalSeconds += EstimateRemainingEncodingTime( 0, GetOutputVideo( V , OutputVideo ) );
                }
            }
            return TotalSeconds;
        }

        int GetQueueTotalFrames()
        {
            int TotalFrames = 0;
            Video OutputVideo = new Video();
            foreach (ListViewItem Item in InputList.Items)
            {
                Video V = (Video)Item.Tag;
                if (V.State == EVideoState.Processing || CanProcessVideo(V))
                {
                    TotalFrames += GetVideoFrames( GetOutputVideo(V, OutputVideo) );
                }
            }
            return TotalFrames;
        }

        int GetQueueProcessedFrames()
        {
            int TotalFrames = 0;
            Video OutputVideo = new Video();
            foreach (ListViewItem Item in InputList.Items)
            {
                Video V = (Video)Item.Tag;
                if (V.State == EVideoState.Processing || CanProcessVideo(V))
                    continue;

                TotalFrames += GetVideoFrames( GetOutputVideo(V, OutputVideo) );
            }

            return TotalFrames + CurrentEncodingFrame;
        }

        void OnTimerQueueUpdate(Object myObject, EventArgs myEventArgs)
        {
            float TotalSeconds = GetQueueEstimatedTime();
            if (TotalSeconds > 0)
            {
                TimeSpan TS = new TimeSpan(0,0,(int)TotalSeconds);
                remainingTimeLabel.Text = TS.ToString(@"hh\:mm\:ss");
            }

            if (IsEncodingProcessRunning())
            {
                if (QueueTotalFrames > 0)
                {
                    int ProcessedFrames = Math.Min(GetQueueProcessedFrames(), QueueTotalFrames);
                    progressBar.Value = (ProcessedFrames *100) / QueueTotalFrames;
                }

                if (CurrentEncodingFPS_MA != null && CurrentEncodingFPS > 0)
                {
                    CurrentEncodingFPS_MA.ComputeAverage(CurrentEncodingFPS);
                }

                float RemainingSeconds = EstimateRemainingEncodingTime( CurrentEncodingFrame, EncodingOutputVideo );
                string RemainingSecondsString = "";
                if (RemainingSeconds > 0)
                {
                    TimeSpan TS = new TimeSpan(0,0,(int)RemainingSeconds);
                    RemainingSecondsString = " Remaining time = " + TS.ToString(@"hh\:mm\:ss");
                }

                lock (StatusLock)
                {
                    if (LastEncoderStatus != null && LastEncoderStatus.Length > 0)
                        StatusText.Text = LastEncoderStatus + RemainingSecondsString;
                }
                return;
            }

            // Encoding is idle, advance queue.

            // If last video was processing, set to completed
            if (EncodingInputVideo != null && EncodingInputVideo.State == EVideoState.Processing)
            {
                if (IsEncodingProcessCompletedSuccessfully())
                {
                    EncodingInputVideo.State = EVideoState.Completed;

                    if (PreserveDates.Checked)
                    {
                        // Change file time
                        FileInfo FI = new FileInfo(EncodingOutputVideo.FilePath);
                        FI.LastWriteTime = EncodingInputVideo.LastWriteTime;
                        FI.LastAccessTime = EncodingInputVideo.LastAccessTime;
                        FI.CreationTime = EncodingInputVideo.CreationTime;
                    }
                }
                else
                {
                    EncodingInputVideo.State = EVideoState.Error;

                }

                RefreshVideoDetails(EncodingInputVideo);

                EncodingInputVideo = null;
            }

            // Start processing next item
            Video V = GetNextVideoToProcess();
            if (V != null)
            {
                StartEncodingProcess(V);
                RefreshVideoDetails(V, true);
            }
            else
            {
                // Queue completed
                StopQueue();
                StatusText.Text = "Done";

            }
        }

        private void ButtonStart_Click(object sender, EventArgs e)
        {
            StartQueue();
        }

        private void ButtonStop_Click(object sender, EventArgs e)
        {
            StopQueue();
        }

        private void RefreshAllVideos()
        {
            Video Output = new Video();
            foreach (ListViewItem Item in InputList.Items)
            {
                Video V = (Video)Item.Tag;
                Item.SubItems.Clear();
                Item.Text = V.FilePath;
                Item.SubItems.Add(GetVideoFormatString(V)); // Input Format
                Item.SubItems.Add(GetVideoFormatString(GetOutputVideo(V, Output))); // Output Format
                Item.SubItems.Add(V.State.ToString());
            }
        }

        private void EnableUI(bool bEnable)
        {
            InputList.Enabled = bEnable;
            noRecompressCheck.Enabled = bEnable;
            resolutionCheck.Enabled = bEnable;
            resolutionList.Enabled = bEnable;
            fpsCheck.Enabled = bEnable;
            fpsList.Enabled = bEnable;
            bitrateCheck.Enabled = bEnable;
            bitrateList.Enabled = bEnable;
            trimCheck.Enabled = bEnable;
            trimList.Enabled = bEnable;
            trimGroup.Enabled = bEnable;
            cropCheck.Enabled = bEnable;
            cropGroup.Enabled = bEnable;
            nvencCheck.Enabled = bEnable;
            hevcCheck.Enabled = bEnable;
            checkNoAudio.Enabled = bEnable;
            buttonClearQueue.Enabled = bEnable;
            buttonResetQueue.Enabled = bEnable;
            encodeBackgroundCheck.Enabled = bEnable;

            // Let UI decides which controls to enable
            if (bEnable)
                RefreshUI();

        }

        private void SkipCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (SkipCheckBox.Checked)
            {
                SkipGroup.Enabled = true;
            } else
            {
                SkipGroup.Enabled = false;
            }
        }
        private void noRecompressCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (noRecompressCheck.Checked)
            {
                resolutionList.SelectedIndex = 0;
                resolutionCheck.Enabled = false;
                resolutionList.Enabled = false;

                fpsCheck.Enabled = false;
                fpsList.SelectedIndex = 0;
                fpsList.Enabled = false;

                bitrateCheck.Enabled = false;
                bitrateList.SelectedIndex = 0;
                bitrateList.Enabled = false;

                cropCheck.Enabled = false;
                cropGroup.Enabled = false;

                nvencCheck.Enabled = false;
                hevcCheck.Enabled = false;
            }
            else
            {
                resolutionCheck.Enabled = true;
                resolutionList.Enabled = resolutionCheck.Checked;

                fpsCheck.Enabled = true;
                fpsList.Enabled = fpsCheck.Checked;

                bitrateCheck.Enabled = true;
                bitrateList.Enabled = bitrateCheck.Checked;

                cropCheck.Enabled = true;
                cropGroup.Enabled = cropCheck.Checked;

                nvencCheck.Enabled = true;
                hevcCheck.Enabled = true;
            }
            RefreshAllVideos();
        }

        private void resolutionCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (resolutionCheck.Checked)
            {
                resolutionList.Enabled = true;
            }
            else
            {
                resolutionList.SelectedIndex = 0;
                resolutionList.Enabled = false;
            }
            RefreshAllVideos();
        }

        private void fpsCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (fpsCheck.Checked)
            {
                fpsList.Enabled = true;
            }
            else
            {
                fpsList.Enabled = false;
                fpsList.SelectedIndex = 0;
            }
            RefreshAllVideos();
        }

        private void RefreshBitrateUI()
        {
            if (bitrateCheck.Checked)
            {
                bitrateList.Enabled = true;
                if (bitrateList.SelectedIndex == 5)
                {
                    bitrateText.Text = "Bitrate Percent %";
                    bitrateText.Visible = true;
                    bitrateValue.Visible = true;
                    bitrateValue.Text = Config.bitratePercent.ToString();
                } else if (bitrateList.SelectedIndex == 6)
                {
                    bitrateText.Text = "Bitrate (kbits)";
                    bitrateText.Visible = true;
                    bitrateValue.Visible = true;
                    bitrateValue.Text = (Config.bitrateValue / 1000).ToString();
                } else
                {
                    bitrateText.Visible = false;
                    bitrateValue.Visible = false;
                }
            }
            else
            {
                bitrateList.Enabled = false;
                //bitrateList.SelectedIndex = 0;
                bitrateText.Visible = false;
                bitrateValue.Visible = false;
            }
        }

        private void bitrateCheck_CheckedChanged(object sender, EventArgs e)
        {
            RefreshBitrateUI();
            RefreshAllVideos();
        }

        private void trimCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (trimCheck.Checked)
            {
                trimList.Enabled = true;
                trimGroup.Enabled = true;
            }
            else
            {
                trimList.Enabled = false;
                trimGroup.Enabled = false;
                trimList.SelectedIndex = 0;
            }


            RefreshAllVideos();
        }

        private void cropCheck_CheckedChanged(object sender, EventArgs e)
        {
            cropGroup.Enabled = cropCheck.Checked;

            RefreshAllVideos();
        }

        private void resolutionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshAllVideos();
        }

        private void fpsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshAllVideos();
        }

        private void bitrateList_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshBitrateUI();
            RefreshAllVideos();
        }

        private void trimList_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (trimList.SelectedIndex)
            {
                case 0:
                    trim2.Text = "Length";
                    break;
                case 1:
                    trim2.Text = "End";
                    break;
            }
            RefreshAllVideos();
        }

        private void ResetQueue()
        {
            if (IsQueueRunning())
                return;
            foreach (ListViewItem Item in InputList.Items)
            {
                Video V = (Video)Item.Tag;
                V.State = EVideoState.Ready;
            }
            RefreshAllVideos();
        }

        private void ClearQueue()
        {
            if (IsQueueRunning())
                return;
            InputList.Items.Clear();
        }

        private void hevcCheck_CheckedChanged(object sender, EventArgs e)
        {
            RefreshAllVideos();
        }

        private void buttonResetQueue_Click(object sender, EventArgs e)
        {
            ClearQueue();
        }

        private void buttonResetQueue_Click_1(object sender, EventArgs e)
        {
            ResetQueue();
        }

        private void SetEncodingProcessPriority()
        {
            if (IsEncodingProcessRunning())
                EncodingProcess.PriorityClass = encodeBackgroundCheck.Checked ? ProcessPriorityClass.Idle : ProcessPriorityClass.Normal;
        }

        private void encodeBackgroundCheck_CheckedChanged(object sender, EventArgs e)
        {
            SetEncodingProcessPriority();
        }

        private void bitrateValue_TextChanged(object sender, EventArgs e)
        {
            if (bitrateList.SelectedIndex == 5)
            {
                int bitratePercent;
                if (Int32.TryParse(bitrateValue.Text, out bitratePercent))
                {
                    bitratePercent = Math.Max(1,bitratePercent);
                    bitratePercent = Math.Min(100,bitratePercent);
                }
                else
                {
                    bitratePercent = 100;
                }
                Config.bitratePercent = bitratePercent;
            }

            if (bitrateList.SelectedIndex == 6)
            {
                int bitrateVal;
                
                if (Int32.TryParse(bitrateValue.Text, out bitrateVal))
                {
                    bitrateVal *= 1000;
                    bitrateVal = Math.Max(1000,bitrateVal);
                    bitrateVal = Math.Min(200*1000*1000,bitrateVal);
                } else
                {
                    bitrateVal = 0;
                }
                Config.bitrateValue = bitrateVal;
            }

            RefreshBitrateUI();
            RefreshAllVideos();
        }

    }

    public class MovingAverage  
    {
        private Queue<float> Samples = new Queue<float>();
        private int WindowSize = 16;
        private float SampleAccumulator;
        public float Average { get; private set; }

        public bool IsWindowComplete() { return Samples.Count == WindowSize; }

        public void ComputeAverage(float newSample)
        {
            SampleAccumulator += newSample;
            Samples.Enqueue(newSample);

            if (Samples.Count > WindowSize)
            {
                SampleAccumulator -= Samples.Dequeue();
            }

            Average = SampleAccumulator / Samples.Count;
        }
    }
}
